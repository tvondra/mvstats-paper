Multivariate Statistics in PostgreSQL (patch)
=============================================

Paper explaining internals of the proposed patch implementing
multivariate statistics in PostgreSQL.
