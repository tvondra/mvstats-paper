drop table t1;
drop table t2;
drop table t3;
drop table t4;
drop table t5;
drop table t6;
drop table t7;
drop table t8;

create table t1 (a float, b float);
create table t2 (a float, b float);
create table t3 (a float, b float);
create table t4 (a float, b float);
create table t5 (a float, b float);
create table t6 (a float, b float);
create table t7 (a float, b float);
create table t8 (a float, b float);

alter table t1 add statistics (histogram) on (a,b);
alter table t2 add statistics (histogram) on (a,b);
alter table t3 add statistics (histogram) on (a,b);
alter table t4 add statistics (histogram) on (a,b);
alter table t5 add statistics (histogram) on (a,b);
alter table t6 add statistics (histogram) on (a,b);
alter table t7 add statistics (histogram) on (a,b);
alter table t8 add statistics (histogram) on (a,b);

-- independent columns, uniform distribution
insert into t1 select random(), random() from generate_series(1,1000000);

-- independent columns, skewed
insert into t2 select random(), random()*random() from generate_series(1,1000000) s(i);

-- correlated columns, diagonal (equal values)
insert into t3 select r,r from (select random() as r from generate_series(1,1000000)) foo;

-- correlated columns (one uniform, the other one a bit lower)
insert into t4 select r*(1-random()/4), r*(1-random()/4) from (select random() as r from generate_series(1,1000000)) foo;

-- correlated columns, skewed distribution
insert into t5 select r, r*random() from (select random() as r from generate_series(1,1000000)) foo;

-- independent columns, skewed distribution
insert into t6 select pow(random(),2), pow(random(),3) from generate_series(1,1000000);

-- independent columns, with a peak in the middle (normal distributions)
insert into t7 select normal_rand(1000000, 0.5, 0.15), normal_rand(1000000, 0.5, 0.15) foo;

-- independent columns, with two peaks
insert into t8 select normal_rand(500000, 0.25, 0.1), normal_rand(500000, 0.25, 0.1) foo;
insert into t8 select normal_rand(500000, 0.75, 0.1), normal_rand(500000, 0.75, 0.1) foo;

analyze t1;
analyze t2;
analyze t3;
analyze t4;
analyze t5;
analyze t6;
analyze t7;
analyze t8;

\t

\o t1.data
select index+1, minvals, maxvals from pg_mv_histogram_buckets((select oid from pg_mv_statistic where starelid = 't1'::regclass), 0);

\o t2.data
select index+1, minvals, maxvals from pg_mv_histogram_buckets((select oid from pg_mv_statistic where starelid = 't2'::regclass), 0);

\o t3.data
select index+1, minvals, maxvals from pg_mv_histogram_buckets((select oid from pg_mv_statistic where starelid = 't3'::regclass), 0);

\o t4.data
select index+1, minvals, maxvals from pg_mv_histogram_buckets((select oid from pg_mv_statistic where starelid = 't4'::regclass), 0);

\o t5.data
select index+1, minvals, maxvals from pg_mv_histogram_buckets((select oid from pg_mv_statistic where starelid = 't5'::regclass), 0);

\o t6.data
select index+1, minvals, maxvals from pg_mv_histogram_buckets((select oid from pg_mv_statistic where starelid = 't6'::regclass), 0);

\o t7.data
select index+1, minvals, maxvals from pg_mv_histogram_buckets((select oid from pg_mv_statistic where starelid = 't7'::regclass), 0);

\o t8.data
select index+1, minvals, maxvals from pg_mv_histogram_buckets((select oid from pg_mv_statistic where starelid = 't8'::regclass), 0);

\o t1.points
select a || ' ' || b from t1 order by random() limit 10000;

\o t2.points
select a || ' ' || b from t2 order by random() limit 10000;

\o t3.points
select a || ' ' || b from t3 order by random() limit 10000;

\o t4.points
select a || ' ' || b from t4 order by random() limit 10000;

\o t5.points
select a || ' ' || b from t5 order by random() limit 10000;

\o t6.points
select a || ' ' || b from t6 order by random() limit 10000;

\o t7.points
select a || ' ' || b from t7 order by random() limit 10000;

\o t8.points
select a || ' ' || b from t8 order by random() limit 10000;
