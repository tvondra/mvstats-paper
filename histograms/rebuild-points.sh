for t in t1 t2 t3 t4 t5 t6 t7 t8; do

	echo "set terminal eps size 4,4" > $t.plot
	echo "set output '$t.eps'" >> $t.plot

	echo "set xrange [0:1]"  >> $t.plot
	echo "set yrange [0:1]" >> $t.plot

	echo "plot '$t.points' with points pt 0 notitle" >> $t.plot

	gnuplot $t.plot

done;
