x="set object \1 rect from \2,\3 to \4,\5 fs border lc rgb '#000000' lw 1"

for t in t1 t2 t3 t4 t5 t6 t7 t8; do

	echo "set terminal eps size 4,4" > $t.plot
	echo "set output '$t.eps'" >> $t.plot

	echo "set xrange [0:1]"  >> $t.plot
	echo "set yrange [0:1]" >> $t.plot

	sed 's/|/ /g' $t.data | \
		sed 's/[ \t]+/ /g' |  sed 's/,/ /g' | sed 's/{/ /g' | sed 's/}/ /g' | \
		sed "s/^\s\+\([0-9]\+\)\s\+\([^ ]\+\)\s\+\([^ ]\+\)\s\+\([^ ]\+\)\s\+\([^ ]\+\)/$x/g" >> $t.plot


	echo "plot [0:1] [0:1] NaN notitle" >> $t.plot

	gnuplot $t.plot

done;
